from .ansi import AnsiFore, AnsiStyle


def init(autoreset: bool) -> None: ...


Fore: AnsiFore
Style: AnsiStyle
