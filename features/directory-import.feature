Feature: Directory Import
  If importing a whole directory, all suitable photos should be identified
  and imported.

  Scenario: Import two photos from the same day
    Two photographs, taken on the same day, and currently sitting alongside
    each other in a directory get imported into the appropriate destination
    directory.

    Given a photo aldwych.jpg taken on 30/9/1994 is to be imported
    And a photo ongar.jpg taken on 30/9/1994 is to be imported
    When photo-import is run
    Then destination directory contains 1994/09/30/aldwych.jpg
    And destination directory contains 1994/09/30/ongar.jpg

  Scenario: Import two photos from different days
    Given a photo la-rosiere.jpg taken on 18/7/2018 is to be imported
    And a photo alpe-dhuez.jpg taken on 19/7/2018 is to be imported
    When photo-import is run
    Then destination directory contains 2018/07/18/la-rosiere.jpg
    And destination directory contains 2018/07/19/alpe-dhuez.jpg

  Scenario: Don't import non-photo files
    Given there is a file irrelevant.txt in the source directory
    When photo-import is run
    Then source directory contains irrelevant.txt
    And output contains "Ignoring irrelevant.txt"