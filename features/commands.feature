Feature: Command line arguments
  Tests the various flags and arguments that can be passed in to the
  photo-import command

  Scenario: Output current version
    When photo-import --version is run
    Then version is output

  Scenario: Import a single file into an empty directory
    Given a photo photo-001.jpg taken on 22/11/2003 is to be imported
    When photo-import is run on photo-001.jpg
    Then destination directory contains 2003/11/22/photo-001.jpg

  Scenario: Import a single photo into the month directory
    If configured to only use the month directory, rather than the full year,
    month and date directory as is the default, the photo should be copied into
    the month directory directly.

    Given a photo photo-001.jpg taken on 11/3/1952 is to be imported
    When photo-import --month-only is run on photo-001.jpg
    Then destination directory contains 1952/03/photo-001.jpg

  Scenario: Dry-run mode does not move files
    Given a photo photo-001.jpg taken on 22/11/2003 is to be imported
    When photo-import --dry-run is run on photo-001.jpg
    Then destination directory does not contain 2003/11/22/photo-001.jpg

  Scenario: Silent mode does not produce any output
    Given a photo photo-001.jpg taken on 22/11/2003 is to be imported
    When photo-import --silent is run on photo-001.jpg
    Then destination directory contains 2003/11/22/photo-001.jpg
    And there are no lines of output
