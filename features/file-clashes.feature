@not-implemented
# Will be implemented in #13
Feature: File name clashes
  If you import a photo with the same name as a photo already in the
  destination directory, the newly imported photo should be given a suffix to
  disambiguate it.

  Scenario: Photo already exists in destination directory
    Given an imported photo photo-001.jpg in directory 2003/11/22
    And a photo photo-001.jpg taken on 22/11/2003 is to be imported
    When photo-import is run on photo-001.jpg
    Then destination directory contains 2003/11/22/photo-001_01.jpg
    And destination directory contains 2003/11/22/photo-001.jpg