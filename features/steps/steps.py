import os
import re
from datetime import datetime
from pathlib import Path
from shutil import copy

import parse
from behave import given, when, then, register_type
from exif import Image

from features.steps.helpers import execute


@parse.with_pattern(r"\d+/\d+/\d+")
def parse_date(text):
    return datetime.strptime(text, "%d/%m/%Y")


SAMPLE_IMAGES_DIR = Path(__file__).absolute().parent.parent
register_type(Date=parse_date)


@given("an imported photo {file_name} in directory {directory}")
def imported_photo(context, file_name, directory):
    source_image = os.path.join(SAMPLE_IMAGES_DIR, "sample-images", "Circle.jpg")
    destination_dir = os.path.join(context.destination_dir.name, directory)
    os.makedirs(destination_dir, exist_ok=True)
    destination = os.path.join(destination_dir, file_name)
    copy(str(source_image), str(destination))


@given("a photo {file_name} taken on {date:Date} is to be imported")
def source_photo(context, file_name, date):
    source = os.path.join(SAMPLE_IMAGES_DIR, "sample-images", "Heart.jpg")
    destination = os.path.join(context.source_dir.name, file_name)

    with open(source, "rb") as image_file:
        modified_image = Image(image_file)
        modified_image["datetime_original"] = date.strftime("%Y:%m:%d %H:%M:%S")
        with open(destination, "wb") as new_image_file:
            new_image_file.write(modified_image.get_file())


@given("there is a companion file {file_name} to be imported")
def source_companion_file(context, file_name):
    file_path = os.path.join(context.source_dir.name, file_name)

    with open(file_path, "w") as companion_file:
        companion_file.write(f"Companion File: {file_name}")


@given("there is a file {file_name} in the source directory")
def source_file(context, file_name):
    file_path = os.path.join(context.source_dir.name, file_name)

    with open(file_path, "w") as companion_file:
        companion_file.write(f"File: {file_name}")


@when("{command} is run")
def run(context, command):
    _, output = execute(command.split() + [str(context.source_dir.name), str(context.destination_dir.name)])
    context.output += output


@when("{command} is run on {file_name}")
def run_on_file(context, command, file_name):
    source_file = os.path.join(context.source_dir.name, file_name)
    _, output = execute(command.split() + [str(source_file), str(context.destination_dir.name)])
    context.output += output


@then("version is output")
def version_is_output(context):
    assert any(re.fullmatch(r"[0-9]+\.[0-9]+\.[0-9]+.*\n?", line) for line in context.output)


@then("there are no lines of output")
def nothing_output(context):
    assert not context.output


@then('output contains "{match}"')
def output_contains(context, match):
    assert any(match in line for line in context.output)


@then("source directory is empty")
def source_is_empty(context):
    assert not os.listdir(context.source_dir.name)


@then("source directory contains {file_name}")
def source_dir_contains(context, file_name):
    path = os.path.join(context.source_dir.name, file_name)
    assert os.path.isfile(path)


@then("destination directory contains {file_name}")
def photo_is_imported(context, file_name):
    path = os.path.join(context.destination_dir.name, file_name)
    assert os.path.isfile(path)


@then("destination directory does not contain {file_name}")
def photo_is_not_imported(context, file_name):
    path = os.path.join(context.destination_dir.name, file_name)
    assert not os.path.isfile(path)
