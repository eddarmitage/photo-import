import tempfile

from features.steps.helpers import execute


def before_all(context):
    execute(["python", "setup.py", "install"])


def before_scenario(context, scenario):
    context.source_dir = tempfile.TemporaryDirectory()
    context.destination_dir = tempfile.TemporaryDirectory()
    context.output = []


def after_scenario(context, scenario):
    context.source_dir.cleanup()
    context.destination_dir.cleanup()
