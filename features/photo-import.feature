Feature: Import individual photo files

  Scenario: File is removed from source directory
    Given a photo dagen-h.jpg taken on 3/9/1967 is to be imported
    When photo-import is run on dagen-h.jpg
    Then destination directory contains 1967/09/03/dagen-h.jpg
    And source directory is empty

  Scenario: Irrelevant file is ignored
    Given there is a file irrelevant.txt in the source directory
    When photo-import is run on irrelevant.txt
    Then source directory contains irrelevant.txt
    And output contains "Ignoring irrelevant.txt"