Feature: Companion Files
  Cameras that shoot in RAW often have the ability to save a JPG rendering of
  the photo as well. Both files typically have the same filename, but with
  different extensions. photo-import should only scan the JPG files, but when
  moving the files into the destination directory, all files with the same name
  (ignoring extension) should be moved.

  Scenario: Import a single photo with its companion file
    Importing a photo should mean that its companion file is also imported
    alongside it.

    Given a photo photo-001.jpg taken on 16/7/1969 is to be imported
    And there is a companion file photo-001.raw to be imported
    When photo-import is run on photo-001.jpg
    Then destination directory contains 1969/07/16/photo-001.jpg
    And destination directory contains 1969/07/16/photo-001.raw

  Scenario: Import a single photo with multiple companion files
    Importing a photo should mean that all of its companion files are also
    imported alongside it.

    Given a photo photo-001.jpg taken on 16/7/1969 is to be imported
    And there is a companion file photo-001.raw to be imported
    And there is a companion file photo-001.dng to be imported
    When photo-import is run on photo-001.jpg
    Then destination directory contains 1969/07/16/photo-001.jpg
    And destination directory contains 1969/07/16/photo-001.raw
    And destination directory contains 1969/07/16/photo-001.dng

