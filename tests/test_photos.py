import csv
import unittest
from datetime import date
from pathlib import Path

from photoimport.photos import read_date

TEST_DIR = Path(__file__).absolute().parent
IMAGE_DIR = TEST_DIR.joinpath("sample-images")


class ExifReaderTest(unittest.TestCase):
    def test_gen(self):
        csv_path = TEST_DIR.joinpath("sample-images.csv")
        with csv_path.open() as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                yield self.check_image, (row["image_file"], int(row["year"]), int(row["month"]), int(row["day"]))

    def check_image(self, file, year, month, day):
        result = read_date(IMAGE_DIR.joinpath(file))
        creation_date = date(year, month, day)
        self.assertEqual(creation_date, result)


if __name__ == "__main__":
    unittest.main()
