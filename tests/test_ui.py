import unittest
from typing import List, Union
from unittest.mock import patch

from colorama import Fore

from photoimport.ui import ConsoleWriter


class FatalOutputTest(unittest.TestCase):
    @patch("builtins.print")
    def test_prefix_printed_for_error(self, mock_print):
        call_fatal_logger(-42, "Error")
        assert_output_contains(mock_print, "❌ A fatal error occurred")

    @patch("builtins.print")
    def test_print_called_for_error(self, mock_print):
        call_fatal_logger(-42, "Error")
        assert_output_contains(mock_print, "Error")

    @patch("builtins.print")
    def test_error_logging_is_red(self, mock_print):
        call_fatal_logger(-42, "Error")
        assert_output_contains(mock_print, Fore.RED)

    @patch("builtins.print")
    def test_error_message_is_indented(self, mock_print):
        call_fatal_logger(-42, "Error")
        assert_output_contains(mock_print, "    ")

    @patch("builtins.print")
    def test_exit_code_is_set(self, mock_print):
        code = call_fatal_logger(-42, "Error")
        self.assertEqual(code, -42)


def call_fatal_logger(code: int, msgs: Union[str, List[str]]) -> int:
    try:
        ConsoleWriter.fatal(code, msgs)
    except SystemExit as e:
        return e.code


class WriterTest(unittest.TestCase):
    def setUp(self) -> None:
        self.writer = ConsoleWriter(False)

    @patch("builtins.print")
    def test_print_called_for_status(self, mock_print):
        self.writer.status("Status")
        assert_output_contains(mock_print, "Status")

    @patch("builtins.print")
    def test_status_is_yellow(self, mock_print):
        self.writer.status("Status")
        assert_output_contains(mock_print, Fore.YELLOW)

    @patch("builtins.print")
    def test_print_called_for_action(self, mock_print):
        self.writer.action("Action")
        assert_output_contains(mock_print, "Action")

    @patch("builtins.print")
    def test_status_is_green(self, mock_print):
        self.writer.action("Action")
        assert_output_contains(mock_print, Fore.GREEN)


class SilentWriterTest(unittest.TestCase):
    def setUp(self) -> None:
        self.writer = ConsoleWriter(True)

    @patch("builtins.print")
    def test_print_called_for_status(self, mock_print):
        self.writer.status("Status")
        self.assertFalse(mock_print.called)

    @patch("builtins.print")
    def test_print_called_for_action(self, mock_print):
        self.writer.action("Action")
        self.assertFalse(mock_print.called)


def assert_output_contains(mock_print, match):
    assert any([args for args, _ in mock_print.call_args_list if any(match in arg for arg in args)])


if __name__ == "__main__":
    unittest.main()
