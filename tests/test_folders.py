import os
import unittest
from datetime import date
from pathlib import Path
from unittest.mock import MagicMock

from pyfakefs import fake_filesystem_unittest

from photoimport.ui import ConsoleWriter
from photoimport.folders import FileMover, find_companion_files, find_all_photos

base_dir = "/destination"
test_date = date(2019, 3, 8)

SILENT_WRITER = ConsoleWriter(True)


class MonthOnlyFileMoverTest(fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.setUpPyfakefs()
        self.mover = FileMover(SILENT_WRITER, base_dir, False, True)
        self.file = _create_file(Path("/"), "file.txt")
        self.destination_dir = os.path.join(base_dir, "2019", "03")
        os.makedirs(base_dir)

    def test_directories_created(self):
        self.mover.create_directory(test_date)
        self._test_directory_exists("2019", "03", "08")

    def test_directories_created_on_blank_drive(self):
        os.removedirs(base_dir)
        self.mover.create_directory(test_date)
        self._test_directory_exists("2019", "03", "08")

    def test_when_year_exists(self):
        os.makedirs(os.path.join(base_dir, "2019"))
        self.mover.create_directory(test_date)
        self._test_directory_exists("2019", "03", "08")

    def test_when_month_exists(self):
        os.makedirs(os.path.join(base_dir, "2019", "03"))
        self.mover.create_directory(test_date)
        self._test_directory_exists("2019", "03", "08")

    def test_file_is_moved(self):
        os.makedirs(self.destination_dir)
        self.mover.move_file(self.file, test_date)

        self.assertFalse(os.path.isfile(self.file))
        self.assertTrue(os.path.isfile(os.path.join(self.destination_dir, "file.txt")))

    def _test_directory_exists(self, year, month, day):
        year_directory = os.path.join(base_dir, year)
        month_directory = os.path.join(year_directory, month)

        self.assertTrue(os.path.exists(base_dir))
        self.assertTrue(os.path.exists(year_directory))
        self.assertTrue(os.path.exists(month_directory))


class MonthDateFileMoverTest(MonthOnlyFileMoverTest):
    def setUp(self):
        super().setUp()
        self.mover = FileMover(SILENT_WRITER, base_dir, False, False)
        self.destination_dir = os.path.join(base_dir, "2019", "03", "08")

    def test_when_date_exists(self):
        os.makedirs(os.path.join(base_dir, "2019", "03", "08"))
        self.mover.create_directory(test_date)
        self._test_directory_exists("2019", "03", "08")

    def _test_directory_exists(self, year, month, day):
        year_directory = os.path.join(base_dir, year)
        month_directory = os.path.join(year_directory, month)
        day_directory = os.path.join(month_directory, day)

        self.assertTrue(os.path.exists(base_dir))
        self.assertTrue(os.path.exists(year_directory))
        self.assertTrue(os.path.exists(month_directory))
        self.assertTrue(os.path.exists(day_directory))


class MonthDryRunFileMoverTest(fake_filesystem_unittest.TestCase):
    def setUp(self) -> None:
        self.setUpPyfakefs()
        self.mover = FileMover(SILENT_WRITER, base_dir, True, True)
        self.file = _create_file(Path("/"), "file.txt")
        self.destination_dir = os.path.join(base_dir, "2019", "03")

    def test_file_is_not_moved(self):
        os.makedirs(self.destination_dir)
        self.mover.move_file(self.file, test_date)

        self.assertTrue(os.path.isfile(self.file))
        self.assertFalse(os.path.isfile(os.path.join(self.destination_dir, "file.txt")))


class MonthDateDryRunFileMoverTest(MonthDryRunFileMoverTest):
    def setUp(self) -> None:
        super().setUp()
        self.mover = FileMover(SILENT_WRITER, base_dir, True, False)
        self.destination_dir = os.path.join(base_dir, "2019", "03", "08")


class FindAllPhotosTest(fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.setUpPyfakefs()
        self.directory = Path(base_dir)
        self.writer = MagicMock(ConsoleWriter)
        os.makedirs(str(self.directory))

    def test_empty_dir(self):
        result = list(find_all_photos(self.directory, self.writer))
        self.assertFalse(result)

    def test_photo_path_provided(self):
        photo = _create_file(self.directory, "photo-001.jpg")
        result = list(find_all_photos(photo, self.writer))

        self.assertTrue(result)
        self.assertEqual(len(result), 1)
        self.assertTrue(photo in result)

    def test_non_existent_photo_path_provided(self):
        photo = self.directory.joinpath("photo-001.jpg")
        result = list(find_all_photos(photo, self.writer))
        self.assertFalse(result)

    def test_non_photo_path_provided(self):
        file = _create_file(self.directory, "irrelevant.txt")
        result = list(find_all_photos(file, self.writer))
        self.assertFalse(result)

    def test_single_photo(self):
        photo = _create_file(self.directory, "photo-001.jpg")
        result = list(find_all_photos(self.directory, self.writer))

        self.assertTrue(result)
        self.assertEqual(len(result), 1)
        self.assertTrue(photo in result)

    def test_non_photo(self):
        _create_file(self.directory, "irrelevant.txt")
        result = list(find_all_photos(self.directory, self.writer))
        self.assertFalse(result)

    def test_multiple_photos(self):
        photo_1 = _create_file(self.directory, "photo-001.jpg")
        photo_2 = _create_file(self.directory, "photo-002.jpg")
        result = list(find_all_photos(self.directory, self.writer))

        self.assertTrue(result)
        self.assertEqual(len(result), 2)
        self.assertTrue(photo_1 in result)
        self.assertTrue(photo_2 in result)


class CompanionFileFinderTest(fake_filesystem_unittest.TestCase):
    def setUp(self):
        self.setUpPyfakefs()
        self.directory = Path(base_dir)
        os.makedirs(str(self.directory))

    def test_no_companions(self):
        photo = _create_file(self.directory, "photo-001.jpg")

        result = list(find_companion_files(photo))
        self.assertEqual(len(result), 1)

        found_file_names = [x.name for x in result]
        self.assertTrue(photo.name in found_file_names)

    def test_single_companion(self):
        photo = _create_file(self.directory, "photo-001.jpg")
        companion = _create_file(self.directory, "photo-001.raw")

        result = list(find_companion_files(photo))
        self.assertEqual(len(result), 2)

        found_file_names = [x.name for x in result]
        self.assertTrue(photo.name in found_file_names)
        self.assertTrue(companion.name in found_file_names)

    def test_multiple_companions(self):
        photo = _create_file(self.directory, "photo-001.jpg")
        companion_1 = _create_file(self.directory, "photo-001.raw")
        companion_2 = _create_file(self.directory, "photo-001.dng")

        result = list(find_companion_files(photo))
        self.assertEqual(len(result), 3)

        found_file_names = [x.name for x in result]
        self.assertTrue(photo.name in found_file_names)
        self.assertTrue(companion_1.name in found_file_names)
        self.assertTrue(companion_2.name in found_file_names)

    def test_with_other_files(self):
        photo = _create_file(self.directory, "photo-001.jpg")
        wrong_photo = _create_file(self.directory, "photo-002.jpg")

        result = list(find_companion_files(photo))
        self.assertEqual(len(result), 1)

        found_file_names = [x.name for x in result]
        self.assertTrue(photo.name in found_file_names)
        self.assertFalse(wrong_photo.name in found_file_names)

    def test_with_substring_file_names(self):
        photo = _create_file(self.directory, "photo-001.jpg")
        wrong_photo = _create_file(self.directory, "photo-0011.jpg")

        result = list(find_companion_files(photo))
        self.assertEqual(len(result), 1)

        found_file_names = [x.name for x in result]
        self.assertTrue(photo.name in found_file_names)
        self.assertFalse(wrong_photo.name in found_file_names)


def _create_file(directory: Path, file_name: str) -> Path:
    os.makedirs(str(directory), exist_ok=True)
    file = directory.joinpath(file_name)
    open(str(file), "a").close()
    return file


if __name__ == "__main__":
    unittest.main()
